#include <stdio.h>
#include <dlfcn.h>
#include <sys/types.h>
#include <linux/elf.h>

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <GLES/gl.h>

#include <android/log.h>

#define ANDROID_ARM_LINKER

struct link_map
{
	uintptr_t l_addr;
	char * l_name;
	uintptr_t l_ld;
	struct link_map * l_next;
	struct link_map * l_prev;
};

typedef struct soinfo soinfo;

#define SOINFO_NAME_LEN 128

struct soinfo
{
	const char name[SOINFO_NAME_LEN];
	Elf32_Phdr *phdr;
	int phnum;
	unsigned entry;
	unsigned base;
	unsigned size;

	int unused;  // DO NOT USE, maintained for compatibility.

	unsigned *dynamic;

	unsigned wrprotect_start;
	unsigned wrprotect_end;

	soinfo *next;
	unsigned flags;

	const char *strtab;
	Elf32_Sym *symtab;

	unsigned nbucket;
	unsigned nchain;
	unsigned *bucket;
	unsigned *chain;

	unsigned *plt_got;

	Elf32_Rel *plt_rel;
	unsigned plt_rel_count;

	Elf32_Rel *rel;
	unsigned rel_count;

	unsigned *preinit_array;
	unsigned preinit_array_count;

	unsigned *init_array;
	unsigned init_array_count;
	unsigned *fini_array;
	unsigned fini_array_count;

	void (*init_func)(void);
	void (*fini_func)(void);

#ifdef ANDROID_ARM_LINKER
	/* ARM EABI section used for stack unwinding. */
	unsigned *ARM_exidx;
	unsigned ARM_exidx_count;
#endif
	unsigned refcount;
	struct link_map linkmap;
};

void (*eglSwapBuffers_addr)(void *, void *);

void eglSwapBuffers_sub(void *display, void *surface)
{
	__android_log_print(ANDROID_LOG_VERBOSE, "payload.so", "eglSwapBuffers_sub");
	eglSwapBuffers_addr(display, surface);
}

int hijack()
{
	eglSwapBuffers_addr = dlsym(RTLD_DEFAULT, "eglSwapBuffers");

	soinfo *elf = dlopen("/system/lib/libsurfaceflinger.so", 0);

	Elf32_Rel  *rel    = elf->plt_rel;
	Elf32_Sym  *symtab = elf->symtab;
	const char *strtab = elf->strtab;

	const char *target = "eglSwapBuffers";

	int i;
	for(i = 0; i < elf->plt_rel_count; i++)
	{
		unsigned sym = ELF32_R_SYM(rel->r_info);
		char *sym_name = NULL;
		unsigned reloc = (unsigned)(elf->base + rel->r_offset);

		if(sym != 0)
		{
			sym_name = (char *)(strtab + symtab[sym].st_name);

			if(!strcmp(target, sym_name))
			{
				*((unsigned *)reloc) = (unsigned)eglSwapBuffers_sub;
				break;
			}
		}

		rel++;
	}

	dlclose(elf);

	return 0;
}
