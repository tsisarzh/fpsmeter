
CC := /home/tsisarzh/Android/Toolchain/bin/arm-linux-androideabi-gcc

BIN := hijack
LIB := payload.so

all: $(BIN) $(LIB)

$(BIN): hijack.c
	$(CC) -o $@ $<

$(LIB): payload.c
	$(CC) -shared -o $@ $< -lGLESv1_CM -llog
